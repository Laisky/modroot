package modroot

import (
	"testing"

	"gitlab.com/Laisky/moda"
	"gitlab.com/Laisky/modb"
)

func TestVers(t *testing.T) {
	sa := moda.Ver() // 1.1
	sb := modb.Ver() // 1.1
	ra := moda.Random
	rb := modb.Random // different with ra

	t.Log(sa, sb, ra, rb)
}
