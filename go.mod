module gitlab.com/Laisky/modroot

go 1.17

require (
	gitlab.com/Laisky/moda v1.0.1
	gitlab.com/Laisky/modb v1.0.2
)

require gitlab.com/Laisky/modc v1.1.0 // indirect
