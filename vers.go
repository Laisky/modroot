package modroot

import (
	"gitlab.com/Laisky/moda"
	"gitlab.com/Laisky/modb"
)

func Vers() (string, string) {
	return moda.Ver(), modb.Ver()
}
